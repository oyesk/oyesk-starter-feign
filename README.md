# Oyesk Starter Feign
This is a custom library made by OYESK team that adds the required and custom configuration for the OpenFeign library through the Maven build tool.

## Features
 * Dependencies for spring cloud, open feign and feign form libraries;
 * Basic configuration of open feign that enables the use of feign clients;
 * Custom exception `FeignOyeskException` and custom exception handler `FeignHttpExceptionHandler` (these implementations can be consulted on the source code);
 * Custom error decoder for feign that throws a `FeignOyeskException` with a `422` response status when an error occurs during the feign requests.

## Version
Current version: **2.6.6**
> **_NOTE:_** The version of this library is always the same as its parent [spring-boot-starter-parent](https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-parent) version

## Usage
 1. Add this into your `pom.xml` to start using `oyesk-starter-feign` dependency:

```xml
<!-- add oyesk-starter-feign to dependencies -->
<dependencies>
    <dependency>
        <groupId>com.oyesk.starter</groupId>
        <artifactId>oyesk-starter-feign</artifactId>
        <version>2.6.6</version>
    </dependency>
</dependencies>
```

 2. You can enable the features of this library by adding this line into your `application.properties`:

```properties
oyesk.feign-config.enabled=true
```

 3. The feign clients that you create should follow the path: `com.oyesk.*.*.client` or `com.oyesk.*.client`, the stars can be replaced by any folder or package name.

## Custom Configuration or Override
This library can be customized to the needs of each project, if you need to change any of the configurations already provided by this library, you can create a `@Bean` with the exact same name and return your own *decoder* / *exception handler*. That will override the library configuration.
This are the `Beans` provided by this library for the Decoder and CustomExceptionHandler (the implementation can be consulted on the library source code): 

```java
    @Bean
    public FeignErrorDecoder customErrorDecoder() { return new FeignErrorDecoder(); }

    @Bean
    public FeignHttpExceptionHandler httpExceptionHandler() { return new FeignHttpExceptionHandler(); }
```

If only want to use this library dependencies you can just disable the configurations on your `application.properties` file, as it was shown before.

If you want to add another configuration related to feign you can create a `@Bean` with your other configurations on a `@Configuration` class.

[//]: # (## Public Domain)

[//]: # (...)

[//]: # ()
[//]: # (## License)

[//]: # (...)

[//]: # ()
[//]: # ()
[//]: # (## Privacy)

[//]: # (...)

[//]: # ()
[//]: # (## Contributing)

[//]: # (...)

[//]: # ()
[//]: # (## Records)

[//]: # (...)

[//]: # ()
[//]: # (## Notices)

[//]: # (...)
