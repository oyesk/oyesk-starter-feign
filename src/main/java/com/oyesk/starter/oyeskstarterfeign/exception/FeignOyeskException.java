package com.oyesk.starter.oyeskstarterfeign.exception;

import com.oyesk.starter.oyeskstartererror.config.ErrorBody;
import feign.FeignException;

public class FeignOyeskException extends FeignException {

    private final ErrorBody errorBody;

    public ErrorBody getErrorBody() {
        return errorBody;
    }

    public FeignOyeskException(int status, String message, ErrorBody errorBody) {
        super(status, message);
        this.errorBody = errorBody;
    }
}
