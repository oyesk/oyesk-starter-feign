package com.oyesk.starter.oyeskstarterfeign.exception;

import com.oyesk.starter.oyeskstartererror.config.ErrorBody;
import com.oyesk.starter.oyeskstartererror.config.HttpExceptionHandler;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class FeignHttpExceptionHandler extends HttpExceptionHandler {

    @ExceptionHandler(FeignOyeskException.class)
    protected ResponseEntity<Object> handleFeignStatusException(FeignOyeskException ex, WebRequest request) {

        var errorBody = new ErrorBody();
        errorBody.setTimestamp(ex.getErrorBody().getTimestamp());
        errorBody.setMessage(ex.getMessage());
        errorBody.setStatus(ex.getErrorBody().getStatus());
        errorBody.setError(ex.getErrorBody().getError());
        errorBody.setDetails(ex.getErrorBody().getDetails());
        errorBody.setPath(ex.getErrorBody().getPath());

        return handleExceptionInternal(ex, errorBody, new HttpHeaders(), HttpStatus.valueOf(ex.status()), request);
    }
}
