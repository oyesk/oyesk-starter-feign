package com.oyesk.starter.oyeskstarterfeign.helper;

//
// Method originally from org.apache.commons -> commons-lang3
// StringUtils.isNotBlank
//

public class StringUtilsHelper {

    public static boolean isNotBlank(CharSequence cs) {
        int strLen = cs == null ? 0 : cs.length();
        if (strLen != 0) {
            for (int i = 0; i < strLen; ++i) {
                if (!Character.isWhitespace(cs.charAt(i))) {
                    return false;
                }
            }
        }
        return true;
    }
}
