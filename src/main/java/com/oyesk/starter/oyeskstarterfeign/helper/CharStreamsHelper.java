package com.oyesk.starter.oyeskstarterfeign.helper;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;

//
// Originally from com.google.common.io
// CharStreams
//

public class CharStreamsHelper {

    public static String toString(Readable r) throws IOException {
        return toStringBuilder(r).toString();
    }

    private static StringBuilder toStringBuilder(Readable r) throws IOException {
        StringBuilder sb = new StringBuilder();
        if (r instanceof Reader) {
            copyReaderToBuilder((Reader) r, sb);
        } else {
            copy(r, sb);
        }

        return sb;
    }

    private static long copyReaderToBuilder(Reader from, StringBuilder to) throws IOException {
        if (from == null || to == null) {
            throw new NullPointerException();
        }

        char[] buf = new char[2048];

        int nRead;
        long total;
        for (total = 0L; (nRead = from.read(buf)) != -1; total += (long) nRead) {
            to.append(buf, 0, nRead);
        }

        return total;
    }

    private static long copy(Readable from, Appendable to) throws IOException {
        if (from instanceof Reader) {
            return to instanceof StringBuilder ? copyReaderToBuilder((Reader) from, (StringBuilder) to) : copyReaderToWriter((Reader) from, asWriter(to));
        } else {
            if (from == null || to == null) {
                throw new NullPointerException();
            }

            long total = 0L;
            CharBuffer buf = createBuffer();

            while (from.read(buf) != -1) {
                buf.flip();
                to.append(buf);
                total += (long) buf.remaining();
                buf.clear();
            }

            return total;
        }
    }

    private static CharBuffer createBuffer() {
        return CharBuffer.allocate(2048);
    }

    private static Writer asWriter(Appendable target) {
        return (Writer) (target instanceof Writer ? (Writer) target : new AppendableWriter(target));
    }

    private static long copyReaderToWriter(Reader from, Writer to) throws IOException {
        if (from == null || to == null) {
            throw new NullPointerException();
        }

        char[] buf = new char[2048];

        int nRead;
        long total;
        for (total = 0L; (nRead = from.read(buf)) != -1; total += (long) nRead) {
            to.write(buf, 0, nRead);
        }

        return total;
    }
}
