package com.oyesk.starter.oyeskstarterfeign.config;

import com.oyesk.starter.oyeskstarterfeign.decoder.FeignErrorDecoder;
import com.oyesk.starter.oyeskstarterfeign.exception.FeignHttpExceptionHandler;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients({"com.oyesk.*.*.client", "com.oyesk.*.client"})
@ConditionalOnMissingBean(annotation = EnableFeignClients.class)
@ConditionalOnProperty(name = "oyesk.feign-config.enabled", havingValue = "true")
public class FeignConfig {

    @Bean
    @ConditionalOnMissingBean(FeignErrorDecoder.class)
    public FeignErrorDecoder customErrorDecoder() {
        return new FeignErrorDecoder();
    }

    @Bean
    public FeignHttpExceptionHandler httpExceptionHandler() {
        return new FeignHttpExceptionHandler();
    }
}
